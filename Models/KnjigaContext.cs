using Microsoft.EntityFrameworkCore;

namespace KnjigeApi.Models {
    public class KnjigaContext : DbContext {
        public KnjigaContext (DbContextOptions<KnjigaContext> options) : base (options) { }
        public DbSet<Knjiga> Knjige { get; set; }
    }
}