using Microsoft.EntityFrameworkCore;

namespace KnjigeApi.Models
{
    public class KnjigeSeeder
    {
        public static void SeedData(IApplicationBuilder builder)
        {
            using (var serviceScope = builder.ApplicationServices.CreateScope())
            {
                KnjigaContext ctx = serviceScope.ServiceProvider.GetService<KnjigaContext>();
                Console.WriteLine("Migracija....");
                ctx.Database.Migrate();
                if (!ctx.Knjige.Any())
                {
                    ctx.Knjige.AddRange(
                        new Knjiga{Avtor="James Cook", Naslov="UX Design Patterns", Isbn="29299292", LetoIzida=2020},
                        new Knjiga{Avtor="Thomas Erl", Naslov=".NET Patterns", Isbn="455544554", LetoIzida=2021},
                        new Knjiga{Avtor="Martin Fowler", Naslov="Microservices", Isbn="76767676", LetoIzida=2022}
                    );
                    ctx.SaveChanges();

                } else {
                    Console.WriteLine("Knjige so že v bazi");
                }
            }
        }
    }
}